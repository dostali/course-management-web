package az.bdc.web.coursemanagementweb;

import java.io.*;
import java.util.List;

import az.bdc.management.dao.User;
import az.bdc.management.service.UserService;
import az.bdc.management.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "user-servlet", value = "/user-servlet")
public class UserServlet extends HttpServlet {


    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        UserService userService = new UserServiceImpl();
        List<User> all = userService.getAll();
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");

        all.forEach(p -> {
            out.println("<br>" + p.getUsername() + " " + p.getPassword() );
        });

        out.println("<br> <a href=\"index.jsp\">back</a> </body> </html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter("phone_number");

        UserService userService = new UserServiceImpl();
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        userService.insert(user);

        resp.sendRedirect("index.jsp");


    }

    public void destroy() {
    }
}