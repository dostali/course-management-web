<%--
  Created by IntelliJ IDEA.
  User: dostali
  Date: 14.05.24
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="assets/css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="user-servlet" method="post">
    <div class="row">
        <div class="left"><label for="username">username</label></div>
        <div class="right"><input type="text" name="username" id="username"></div>
    </div>
    <div class="row">
        <div class="left"><label for="password">password</label></div>
        <div class="right"><input type="text" name="password" id="password"></div>
    </div>
    <div class="row">
        <div class="left"><label for="email">email</label></div>
        <div class="right"><input type="text" name="email" id="email"></div>
    </div>
    <div class="row">
        <div class="left"><label for="phone_number">phone_number</label></div>
        <div class="right"><input type="text" name="phone_number" id="phone_number"></div>
    </div>
    <div class="row">
        <div class="left"><input type="submit" name="add-user" value="add-user"></div>
    </div>
</form>

</body>
</html>
